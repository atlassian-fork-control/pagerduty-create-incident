import os
import shutil
import subprocess

import docker
from bitbucket_pipes_toolkit.test import PipeTestCase


DOCKER_STUB_CONTAINER_ID = None
PAGERDUTY_STUB_IP = None
REQUESTS_CA_BUNDLE = '.ssl/certificate.pem'


docker_image = (
    f'bitbucketpipelines/demo-pipe-python:ci'
    f'{os.getenv("BITBUCKET_BUILD_NUMBER", "local")}'
)

docker_image_stub = (
    f'bitbucketpipelines/demo-pipe-python:ci'
    f'-stub-'
    f'{os.getenv("BITBUCKET_BUILD_NUMBER", "local")}'
)


docker_client = docker.from_env()
cwd = os.getcwd()


def docker_build():
    """
    Build the docker image for tests.
    :return:
    """
    args = [
        'docker',
        'build',
        '-f',
        'Dockerfile',
        '-t',
        docker_image,
        '.',
    ]
    subprocess.run(args, check=True)


def docker_build_stub():
    """
    Build the docker stub image for tests.
    :return:
    """
    os.chdir('test/pagerduty-stub')
    args = [
        'docker',
        'build',
        '-t',
        docker_image_stub,
        '.',
    ]
    subprocess.run(args, check=True)
    os.chdir(cwd)


def get_docker_ip_address(docker_client, container_id):
    return docker_client.api.inspect_container(container_id)['NetworkSettings']['IPAddress']


def run_stub():
    stub_container = docker_client.containers.run(
        docker_image_stub,
        environment=[
            f'API_KEY={os.getenv("API_KEY")}',
            f'EMAIL={os.getenv("EMAIL")}',
            f'SERVICE_ID={os.getenv("SERVICE_ID")}',
        ],
        ports={'443/tcp': '443'},
        detach=True
    )
    global DOCKER_STUB_CONTAINER_ID
    DOCKER_STUB_CONTAINER_ID = stub_container.id


class PagerDutyCreateIncidentTestCase(PipeTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # ssl processing
        clean_up_ssl()
        os.makedirs(".ssl", exist_ok=True)
        create_ssl_certificate()
        shutil.copytree('.ssl', 'test/pagerduty-stub/.ssl')

        docker_build()
        docker_build_stub()
        run_stub()

        global PAGERDUTY_STUB_IP
        PAGERDUTY_STUB_IP = get_docker_ip_address(docker_client, DOCKER_STUB_CONTAINER_ID)

    def test_no_parameters(self):
        result = self.run_container(environment={})
        self.assertRegex(
            result, '✖ Validation errors: \nAPI_KEY:\n- required field')

    def test_no_email(self):
        result = self.run_container(environment={
            'API_KEY': os.getenv("API_KEY"),
        })
        self.assertRegex(
            result, '✖ Validation errors: \nEMAIL:\n- required field')

    def test_no_service_id(self):
        result = self.run_container(environment={
            'API_KEY': os.getenv("API_KEY"),
            'EMAIL': os.getenv("EMAIL"),
        })
        self.assertRegex(
            result, '✖ Validation errors: \nSERVICE_ID:\n- required field')

    def test_wrong_service_id(self):
        result = self.run_container(
            environment={
                'REQUESTS_CA_BUNDLE': REQUESTS_CA_BUNDLE,
                'API_KEY': os.getenv("API_KEY"),
                'EMAIL': os.getenv("EMAIL"),
                'SERVICE_ID': 'wrong_service_id',
            },
            extra_hosts={
                'api.pagerduty.com': f'{PAGERDUTY_STUB_IP}',
            }
        )
        self.assertIn(
            'Failed to create an incident in PagerDuty', result)
        self.assertIn(
            'Service id must be a valid ID.', result)

    def test_wrong_api_key(self):
        result = self.run_container(
            environment={
                'REQUESTS_CA_BUNDLE': REQUESTS_CA_BUNDLE,
                'API_KEY': 'wrong_api_key',
                'EMAIL': os.getenv("EMAIL"),
                'SERVICE_ID': os.getenv("SERVICE_ID"),
            },
            extra_hosts={
                'api.pagerduty.com': f'{PAGERDUTY_STUB_IP}',
            }
        )
        self.assertIn(
            'Failed to create an incident in PagerDuty', result)
        self.assertIn(
            'You did not supply credentials', result)

    def test_success(self):
        result = self.run_container(
            environment={
                'REQUESTS_CA_BUNDLE': REQUESTS_CA_BUNDLE,
                'API_KEY': os.getenv("API_KEY"),
                'EMAIL': os.getenv("EMAIL"),
                'SERVICE_ID': os.getenv("SERVICE_ID"),
                'DESCRIPTION': 'Test incident',
            },
            extra_hosts={
                'api.pagerduty.com': f'{PAGERDUTY_STUB_IP}',
            }
        )
        self.assertIn(
            'Incident successfully created', result)

    def test_debug(self):
        result = self.run_container(
            environment={
                'REQUESTS_CA_BUNDLE': REQUESTS_CA_BUNDLE,
                'API_KEY': os.getenv("API_KEY"),
                'EMAIL': os.getenv("EMAIL"),
                'SERVICE_ID': os.getenv("SERVICE_ID"),
                'DESCRIPTION': 'Test incident',
                'DEBUG': 'true',
            },
            extra_hosts={
                'api.pagerduty.com': f'{PAGERDUTY_STUB_IP}',
            }
        )
        self.assertIn(
            'Incident successfully created', result)

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

        try:
            docker_client.containers.get(DOCKER_STUB_CONTAINER_ID).stop()
        except docker.errors.NotFound:
            print('No containers to stop')


def clean_up_ssl():
    shutil.rmtree('.ssl/', ignore_errors=True)
    shutil.rmtree('test/pagerduty-stub/.ssl/', ignore_errors=True)


def create_ssl_certificate():
    # openssl req -newkey rsa:2048 -nodes -keyout .ssl/key.pem -x509 -days 365 -out .ssl/certificate.pem
    args = [
        'openssl',
        'req',
        '-newkey',
        'rsa:2048',
        '-nodes',
        '-keyout',
        '.ssl/key.pem',
        '-x509',
        '-days',
        '365',
        '-out',
        REQUESTS_CA_BUNDLE,
        '-subj',
        '/C=AU/ST=AU/L=AU/O=AU/CN=api.pagerduty.com',
    ]
    subprocess.run(args)
